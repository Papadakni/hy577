# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 13:15:08 2021

@author: Papadakni
"""

#This code is used to generate insights for the novel coronavirus (Sars Cov2)
#using a dataset originating from Hospital Israelita Albert Einstein,
#at S~ao Paulo, Brazil, and who had samples collected to perform the
#SARS-CoV-2 RT-PCR and additional laboratory tests during a visit to the
#hospital. This project is done to fullfill the requirements of the 
#class HY577- machine learning of university of crete and thus doesn't 
#represent a laboratory quality evaluation therefore its results and code
#should be evaluated and used as such.

#Import Machine learning Libraries

import numpy  as np
import pandas as pd

import matplotlib.pyplot as plt


from pdpbox import pdp, get_dataset, info_plots
from sklearn.inspection import permutation_importance
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
# for exhaustive search over specified parameter values for an estimator
from sklearn.model_selection import GridSearchCV
import eli5 #for purmutation importance
from eli5.sklearn import PermutationImportance
from sklearn.ensemble import RandomForestClassifier #for the model
import shap #for SHAP values
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve, auc #for model evaluation
from sklearn.naive_bayes import CategoricalNB


from sklearn.neighbors import KNeighborsClassifier

from sklearn.model_selection import train_test_split
from sklearn.metrics         import roc_auc_score, accuracy_score


#Initial Read of the Dataset

Covid19_dataset=pd.read_excel("dataset.xlsx")

#We can see that the dataset has a lot of NaN/null values and we have to 
#deal with that in some way (It makes sense since not everybody had the same
#blood tests in the hospital)

#print(Covid19_dataset.sample(3))

# Count NaN values per column
nan_df = pd.DataFrame(Covid19_dataset.isna().sum()).reset_index()
nan_df.columns  = ['Column', 'NaN_Count']
nan_df['NaN_Count'] = nan_df['NaN_Count'].astype('int')
nan_df['NaN_%'] = round(nan_df['NaN_Count']/5644 * 100,1)
nan_df['Type']  = 'Missingness'
nan_df.sort_values('NaN_%', inplace=True)
# Add completeness
for i in range(nan_df.shape[0]):
    complete_df = pd.DataFrame([nan_df.loc[i,'Column'],Covid19_dataset.shape[0] - nan_df.loc[i,'NaN_Count'],100 - nan_df.loc[i,'NaN_%'], 'Completeness']).T
    complete_df.columns  = ['Column','NaN_Count','NaN_%','Type']
    complete_df['NaN_%'] = complete_df['NaN_%'].astype('int')
    complete_df['NaN_Count'] = complete_df['NaN_Count'].astype('int')
    nan_df = nan_df.append(complete_df, sort=True)

# Missingness Plot
plt.figure()
nan_df.plot.bar(x='Column', y=['NaN_%'])
plt.title("Missingness")
plt.xticks(np.arange(0, 222, step=10))
plt.show()
plt.close()

#Principal Component Analysis

#Conversion from categorical to numerical
mask = {'positive': 1, 
        'negative': 0,
        'detected': 1, 
        'not_detected': 0,
        'not_done': np.NaN,
        'Não Realizado': np.NaN,
        'absent': 0, 
        'present': 1,
        'detected': 1, 
        'not_detected': 0,
        'normal': 1,
        'light_yellow': 1, 
        'yellow': 2, 
        'citrus_yellow': 3, 
        'orange': 4,
        'clear': 1, 
        'lightly_cloudy': 2, 
        'cloudy': 3, 
        'altered_coloring': 4,
        '<1000': 1000,
        'Ausentes': 0, 
        'Urato Amorfo --+': 1, 
        'Oxalato de Cálcio +++': 1,
        'Oxalato de Cálcio -++': 1, 
        'Urato Amorfo +++': 1}


Covid19_dataset_numeric = Covid19_dataset.replace(mask)

Covid19_dataset_numeric['Urine - pH'] = Covid19_dataset_numeric['Urine - pH'].astype('float')
Covid19_dataset_numeric['Urine - Leukocytes'] = Covid19_dataset_numeric['Urine - Leukocytes'].astype('float')

#print(Covid19_dataset_numeric)
#data_top = Covid19_dataset.head()
#print(data_top) 

#remove patient id
Covid19_dataset_numeric=Covid19_dataset_numeric.drop(columns=["Patient ID"])

#replace nan with 0
#Covid19_dataset_numeric= Covid19_dataset_numeric.fillna(0)


#separate the data by test type
red_white_blood_cells_test=Covid19_dataset_numeric[['SARS-Cov-2 exam result',
                                                    'Hematocrit',
                                                    'Hemoglobin',
                                                    'Platelets',
                                                    'Mean platelet volume ',
                                                    'Red blood Cells','Red blood cell distribution width (RDW)',
                                                    'Mean corpuscular hemoglobin (MCH)',
                                                    'Mean corpuscular volume (MCV)',
                                                    'Leukocytes',
                                                    'Lymphocytes',
                                                    'Basophils',
                                                    'Eosinophils',
                                                    'Neutrophils',
                                                    'Monocytes',
                                                    ]]
#clear na values (a.k.a not having this test done)
red_white_blood_cells_test=red_white_blood_cells_test.dropna()

#data preparation

# defining target variables 
target = red_white_blood_cells_test['SARS-Cov-2 exam result']

# defining predictor variables 
features = red_white_blood_cells_test.drop(columns=["SARS-Cov-2 exam result"])

# assigning the splitting of data into respective variables
X_train,X_test,y_train,y_test = train_test_split(features, target, test_size=0.3, random_state=42, stratify = target)



#machine learning algorithms first dataset 

#KNN CLASSIFIER

# instantiating KNeighborsClassifier
knn = KNeighborsClassifier()

# assigning the dictionary of variables whose optimium value is to be retrieved
param_grid = {'n_neighbors' : np.arange(1,50)}

# performing Grid Search CV on knn-model, using 5-cross folds for validation of each criteria
knn_cv = GridSearchCV(knn, param_grid, cv=5)

# training the model with the training data and best parameter
knn_cv.fit(X_train,y_train)

# predicting the values using the testing data set
y_pred = knn_cv.predict(X_test)

print("The score accuracy for training data (KNN 1st Dataset) is: {}" .format(knn_cv.score(X_train,y_train)))
print("The score accuracy for testing data (KNN 1st Dataset) is: {}" .format(knn_cv.score(X_test,y_test)))


y_scores = knn_cv.predict_proba(X_test)
fpr, tpr, threshold = roc_curve(y_test, y_scores[:, 1])
roc_auc = auc(fpr, tpr)

plt.title('Receiver Operating Characteristic')
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of kNN')
plt.show()
plt.close()


#permutation importance KNN

perm = PermutationImportance(knn_cv, random_state=1).fit(X_test, y_test)
eli5.show_weights(perm, feature_names = X_test.columns.tolist())

# Store feature weights in an object
html_obj = eli5.show_weights(perm, feature_names = X_test.columns.tolist())

# Write html object to a file (adjust file path; Windows path is used here)
with open('.\importance_knn1.htm','wb') as f:
    f.write(html_obj.data.encode("UTF-8"))


knn_explainer = shap.KernelExplainer(knn_cv.predict,X_test)

#uncomment on final run cause it takes a lot of time
knn_shap_values = knn_explainer.shap_values(X_test)
#shap.summary_plot(knn_shap_values[1], X_test, plot_type="bar")
shap.summary_plot(knn_shap_values, X_test)

#Lets tree random forest
model = RandomForestClassifier(max_depth=10)
model.fit(X_train, y_train)

y_predict = model.predict(X_test)
y_pred_quant = model.predict_proba(X_test)[:, 1]
y_pred_bin = model.predict(X_test)

confusion_matrix2 = confusion_matrix(y_test, y_pred_bin)


total=sum(sum(confusion_matrix2))

sensitivity = confusion_matrix2[0,0]/(confusion_matrix2[0,0]+confusion_matrix2[1,0])
print('Sensitivity : ', sensitivity )

specificity = confusion_matrix2[1,1]/(confusion_matrix2[1,1]+confusion_matrix2[0,1])
print('Specificity : ', specificity)

fpr, tpr, thresholds = roc_curve(y_test, y_pred_quant)

fig, ax = plt.subplots()
ax.plot(fpr, tpr)
ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls="--", c=".3")
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.0])
plt.rcParams['font.size'] = 12
plt.title('ROC curve for random forest red/white cells blood test classifier')
plt.xlabel('False Positive Rate (1 - Specificity)')
plt.ylabel('True Positive Rate (Sensitivity)')
plt.grid(True)
plt.show()
plt.close()


explainer = shap.TreeExplainer(model)
shap_values = explainer.shap_values(X_test)

shap.summary_plot(shap_values, X_test)


#partial dependance plot

base_features = features.columns.values.tolist()


feat_name = 'Leukocytes'
pdp_dist = pdp.pdp_isolate(model=model, dataset=X_test, model_features=base_features, feature=feat_name)

pdp.pdp_plot(pdp_dist, feat_name)
plt.show()


feat_name = 'Eosinophils'
pdp_dist = pdp.pdp_isolate(model=model, dataset=X_test, model_features=base_features, feature=feat_name)

pdp.pdp_plot(pdp_dist, feat_name)
plt.show()

#permutation importance random forest

perm = PermutationImportance(model, random_state=1).fit(X_test, y_test)
eli5.show_weights(perm, feature_names = X_test.columns.tolist())

# Store feature weights in an object
html_obj = eli5.show_weights(perm, feature_names = X_test.columns.tolist())

# Write html object to a file (adjust file path; Windows path is used here)
with open('.\importance_random_forest1.htm','wb') as f:
    f.write(html_obj.data.encode("UTF-8"))


#logistic regression dataset 1

logreg = LogisticRegression(class_weight='balanced')
logreg.fit(X_train,y_train)



y_predict = logreg.predict(X_test)
y_pred_quant = logreg.predict_proba(X_test)[:, 1]
y_pred_bin = logreg.predict(X_test)

confusion_matrix3 = confusion_matrix(y_test, y_pred_bin)


total=sum(sum(confusion_matrix3))

sensitivity = confusion_matrix3[0,0]/(confusion_matrix3[0,0]+confusion_matrix3[1,0])
print('Sensitivity : ', sensitivity )

specificity = confusion_matrix3[1,1]/(confusion_matrix3[1,1]+confusion_matrix3[0,1])
print('Specificity : ', specificity)

fpr, tpr, thresholds = roc_curve(y_test, y_pred_quant)

fig, ax = plt.subplots()
ax.plot(fpr, tpr)
ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls="--", c=".3")
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.0])
plt.rcParams['font.size'] = 12
plt.title('ROC curve for log regression red/white cells blood test classifier')
plt.xlabel('False Positive Rate (1 - Specificity)')
plt.ylabel('True Positive Rate (Sensitivity)')
plt.grid(True)
plt.show()
plt.close()

#permutation importance logistic regression

perm = PermutationImportance(logreg, random_state=1).fit(X_test, y_test)
eli5.show_weights(perm, feature_names = X_test.columns.tolist())

# Store feature weights in an object
html_obj = eli5.show_weights(perm, feature_names = X_test.columns.tolist())

# Write html object to a file (adjust file path; Windows path is used here)
with open('.\importance_logreg_1.htm','wb') as f:
    f.write(html_obj.data.encode("UTF-8"))


log_reg_explainer = shap.KernelExplainer(logreg.predict,X_test)

#uncomment on final run cause it takes a lot of time
#logreg_shap_values = log_reg_explainer.shap_values(X_test)
#shap.summary_plot(logreg_shap_values[1], X_test, plot_type="bar")
#shap.summary_plot(logreg_shap_values, X_test)


#dataset 2 other virus testing

#Respiratory virus and bacteria Tests

#separate the data by test type
viruses_tests=Covid19_dataset_numeric[[
'SARS-Cov-2 exam result',    
'Rhinovirus/Enterovirus',	
'Inf A H1N1 2009', 
'Influenza B',	
'CoronavirusNL63',	
'Coronavirus HKU1',	
'Coronavirus229E',	
'Influenza A',	
'Respiratory Syncytial Virus',	
'Metapneumovirus',	
'Parainfluenza 1', 
'Parainfluenza 2',
'Parainfluenza 3', 
'Parainfluenza 4', 
'CoronavirusOC43',	
'Bordetella pertussis',	
'Adenovirus',
'Chlamydophila pneumoniae',		
]]
#clear na values (a.k.a not having this test done)
viruses_tests=viruses_tests.dropna()

#data preparation
# defining target variables 
target = viruses_tests['SARS-Cov-2 exam result']

# defining predictor variables 
features = viruses_tests.drop(columns=["SARS-Cov-2 exam result"])

# assigning the splitting of data into respective variables
X_train,X_test,y_train,y_test = train_test_split(features, target, test_size=0.3, random_state=42, stratify = target)


# instantiating KNeighborsClassifier
knn2 = KNeighborsClassifier()

# assigning the dictionary of variables whose optimium value is to be retrieved
param_grid2 = {'n_neighbors' : np.arange(1,100)}

# performing Grid Search CV on knn-model, using 5-cross folds for validation of each criteria
knn_cv2 = GridSearchCV(knn2, param_grid2, cv=5)

# training the model with the training data and best parameter
knn_cv2.fit(X_train,y_train)

# predicting the values using the testing data set
y_pred = knn_cv2.predict(X_test)

print("The score accuracy for training data (KNN 1st Dataset) is: {}" .format(knn_cv2.score(X_train,y_train)))
print("The score accuracy for testing data (KNN 1st Dataset) is: {}" .format(knn_cv2.score(X_test,y_test)))


y_scores = knn_cv2.predict_proba(X_test)
fpr, tpr, threshold = roc_curve(y_test, y_scores[:, 1])
roc_auc = auc(fpr, tpr)

plt.title('Receiver Operating Characteristic')
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of kNN2')
plt.show()
plt.close()


#permutation importance KNN

perm = PermutationImportance(knn_cv2, random_state=1).fit(X_test, y_test)
eli5.show_weights(perm, feature_names = X_test.columns.tolist())

# Store feature weights in an object
html_obj = eli5.show_weights(perm, feature_names = X_test.columns.tolist())

# Write html object to a file (adjust file path; Windows path is used here)
with open('.\importance_knn2.htm','wb') as f:
    f.write(html_obj.data.encode("UTF-8"))


knn_explainer = shap.KernelExplainer(knn_cv2.predict,X_test)

#uncomment on final run cause it takes a lot of time
#knn_shap_values = knn_explainer.shap_values(X_test)
#shap.summary_plot(knn_shap_values[1], X_test, plot_type="bar")
#shap.summary_plot(knn_shap_values, X_test)



#lets try naive bayes

#clf = CategoricalNB()

# training the model with the training data and best parameter
#clf.fit(X_train,y_train)

# predicting the values using the testing data set
#y_pred = clf.predict(X_test)

#print("The score accuracy for training data (bayes 2nd Dataset) is: {}" .format(clf.score(X_train,y_train)))
#print("The score accuracy for testing data (bayes 2nd Dataset) is: {}" .format(clf.score(X_test,y_test)))


#y_scores = clf.predict_proba(X_test)
#fpr, tpr, threshold = roc_curve(y_test, y_scores[:, 1])
#roc_auc = auc(fpr, tpr)

#plt.title('Receiver Operating Characteristic')
#plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
#plt.legend(loc = 'lower right')
#plt.plot([0, 1], [0, 1],'r--')
#plt.xlim([0, 1])
#plt.ylim([0, 1])
#plt.ylabel('True Positive Rate')
#plt.xlabel('False Positive Rate')
#plt.title('ROC Curve of Naive bayes 2')
#plt.show()
#plt.close()

#imps = permutation_importance(clf, X_test, y_test)
#print(imps.importances_mean)



#Lets tree random forest
model = RandomForestClassifier(max_depth=10)
model.fit(X_train, y_train)

y_predict = model.predict(X_test)
y_pred_quant = model.predict_proba(X_test)[:, 1]
y_pred_bin = model.predict(X_test)

confusion_matrix2 = confusion_matrix(y_test, y_pred_bin)


total=sum(sum(confusion_matrix2))

sensitivity = confusion_matrix2[0,0]/(confusion_matrix2[0,0]+confusion_matrix2[1,0])
print('Sensitivity : ', sensitivity )

specificity = confusion_matrix2[1,1]/(confusion_matrix2[1,1]+confusion_matrix2[0,1])
print('Specificity : ', specificity)

fpr, tpr, thresholds = roc_curve(y_test, y_pred_quant)

fig, ax = plt.subplots()
ax.plot(fpr, tpr)
ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls="--", c=".3")
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.0])
plt.rcParams['font.size'] = 12
plt.title('ROC curve for random forest viruses tests classifier')
plt.xlabel('False Positive Rate (1 - Specificity)')
plt.ylabel('True Positive Rate (Sensitivity)')
plt.grid(True)
plt.show()
plt.close()


explainer = shap.TreeExplainer(model)
shap_values = explainer.shap_values(X_test)

shap.summary_plot(shap_values, X_test)


#permutation importance random forest

perm = PermutationImportance(model, random_state=1).fit(X_test, y_test)
eli5.show_weights(perm, feature_names = X_test.columns.tolist())

# Store feature weights in an object
html_obj = eli5.show_weights(perm, feature_names = X_test.columns.tolist())

# Write html object to a file (adjust file path; Windows path is used here)
with open('.\importance_random_forest2.htm','wb') as f:
    f.write(html_obj.data.encode("UTF-8"))


#partial dependance plot

base_features = features.columns.values.tolist()

feat_name = 'Rhinovirus/Enterovirus'
pdp_dist = pdp.pdp_isolate(model=model, dataset=X_test, model_features=base_features, feature=feat_name)

pdp.pdp_plot(pdp_dist, feat_name)
plt.show()


#logistic regression dataset 1

logreg = LogisticRegression(class_weight='balanced')
logreg.fit(X_train,y_train)



y_predict = logreg.predict(X_test)
y_pred_quant = logreg.predict_proba(X_test)[:, 1]
y_pred_bin = logreg.predict(X_test)

confusion_matrix3 = confusion_matrix(y_test, y_pred_bin)


total=sum(sum(confusion_matrix3))

sensitivity = confusion_matrix3[0,0]/(confusion_matrix3[0,0]+confusion_matrix3[1,0])
print('Sensitivity : ', sensitivity )

specificity = confusion_matrix3[1,1]/(confusion_matrix3[1,1]+confusion_matrix3[0,1])
print('Specificity : ', specificity)

fpr, tpr, thresholds = roc_curve(y_test, y_pred_quant)

fig, ax = plt.subplots()
ax.plot(fpr, tpr)
ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls="--", c=".3")
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.0])
plt.rcParams['font.size'] = 12
plt.title('ROC curve for log regression viruses tests classifier')
plt.xlabel('False Positive Rate (1 - Specificity)')
plt.ylabel('True Positive Rate (Sensitivity)')
plt.grid(True)
plt.show()
plt.close()

#permutation importance logistic regression

perm = PermutationImportance(logreg, random_state=1).fit(X_test, y_test)
eli5.show_weights(perm, feature_names = X_test.columns.tolist())

# Store feature weights in an object
html_obj = eli5.show_weights(perm, feature_names = X_test.columns.tolist())

# Write html object to a file (adjust file path; Windows path is used here)
with open('.\importance_logreg_2.htm','wb') as f:
    f.write(html_obj.data.encode("UTF-8"))

log_reg_explainer = shap.KernelExplainer(logreg.predict,X_test)

#uncomment on final run cause it takes a lot of time
#logreg_shap_values = log_reg_explainer.shap_values(X_test)
#shap.summary_plot(logreg_shap_values[1], X_test, plot_type="bar")
#shap.summary_plot(logreg_shap_values, X_test)


